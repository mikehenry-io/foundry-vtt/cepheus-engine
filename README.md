# A·C·E | Advanced Cepheus Engine – Foundry VTT Game System

This game system for [Foundry Virtual Tabletop](http://foundryvtt.com) provides character sheets and game system support for the A·C·E | Advanced Cepheus Engine RPG.

This system is offered and may be used under the terms of the Open Gaming License v1.0a and its accompanying
[Cepheus Engine System Reference Document](https://www.orffenspace.com/cepheus-srd/).

This system provides character sheet support for Actors and Items, mechanical support for dice and rules necessary to play A·C·E games, and compendium content for Characters, Aliens, Items, and more!

Data present under the `packs/` directory is taken from the
[Cepheus Engine System Reference Document](https://www.orffenspace.com/cepheus-srd/)
and used under the terms of the OGL v1.0a, see OGL.md.

Images present under the `icons/` directory are distributed under various terms, please see the [`icons/LICENSE.md`](icons/LICENSE.md) file for full details.

The software component of this system is distributed under the MIT license.

## Installation Instructions

To install and use the A·C·E game system for Foundry VTT, exit out of any game world back to the **Configuration and Setup** screen and select the **Game Systems** tab. Click on the **Install System** button at the bottom of the panel and paste the URL below into the **Manifest URL** text-box at bottom of the dialog.

https://gitlab.com/mikehenry-io/foundry-vtt/ace/-/releases/permalink/latest/downloads/system.json

If you wish to manually install the system, you must clone or extract it into the `Data/systems/ACE` folder. You may do this by cloning the repository or downloading a zip archive from the [Releases Page](https://gitlab.com/mikehenry-io/foundry-vtt/ace/-/releases).

## Community Contribution

See the [CONTRIBUTING](/CONTRIBUTING.md) file for information about how you can help this project.
