/**
 * A simple form to set actor movement speeds.
 */
export default class ActorMovementConfig extends DocumentSheet {
  constructor(...args) {
    super(...args);

    /**
     * Cloned copy of the actor for previewing changes.
     * @type {ACE_Actor}
     */
    this.clone = this.object.clone();
  }

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "actor-movement-config",
      classes: ["ACE"],
      template: "systems/ACE/templates/apps/movement-config.hbs",
      width: 300,
      height: "auto"
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get title() {
    return `${game.i18n.localize("ACE.MovementConfig")}: ${this.document.name}`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const system = this.clone.system;
    let movement = system.attributes.movement;
    switch (system.attributes.loco) {
      case "bipedal":
        movement = {
          burrow: 0,
          climb: 0,
          fly: 0,
          swim: 1.5,
          walk: 1.5 * (4 + system.characteristics.dex.mod),
          units: "m",
          hover: false
        };
        break;
      case "quadrupedal":
      case "hexapedal":
        break;
    }

    const data = {
      disable: Object.keys(CONFIG.ACE.locoNames).includes(system.attributes.loco),
      loco: system.attributes.loco,
      locoTypes: CONFIG.ACE.locoTypes,
      movement: movement,
      units: CONFIG.ACE.movementUnits
    };
    for ( let [k, v] of Object.entries(data.movement) ) {
      if ( ["units", "hover"].includes(k) ) continue;
      data.movement[k] = Number.isNumeric(v) ? v.toNearest(0.1) : 0;
    }

    console.log("A·C·E | ActorMovementConfig.getData():", this, system, data);

    return data;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    const form = foundry.utils.expandObject(formData);
    const data = {
      "system.attributes.loco": form.loco,
      "system.attributes.movement": form.movement
    };

    console.log("A·C·E | ActorMovementConfig._updateObject():",
      this, event, formData, form, data);

    return this.object.update(data);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onChangeInput(event) {
    await super._onChangeInput(event);

    console.log("A·C·E | ActorMovementConfig._onChangeInput():", this, event);

    // Update clone with new data & re-render
    this.clone.updateSource({ [`system.attributes.${event.currentTarget.name}`]: event.currentTarget.value });
    this.render();
  }
}
