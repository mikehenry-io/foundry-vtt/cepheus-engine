import { ACE_CharacterSheet } from "./character.mjs";

/**
 * An Actor sheet for npc type actors.
 * @extends {ACE_CharacterSheet}
 */
export class ACE_NPCSheet extends ACE_CharacterSheet {
  /**
   * Define default rendering options for the NPC sheet.
   * @returns {object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "actor", "npc"],
      height: 645
    });
  }

  // --------------------------------------------

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   * @param {object} options
   * @returns {object}  Prepared copy of the actor data ready to be displayed.
   */
  async getData(options={}) {
    return mergeObject(await super.getData(options), {
      isCharacter: false,
      isNPC: true
    });
  }
}
