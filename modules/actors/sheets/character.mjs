import { ACE_ActorSheet } from "./actor.mjs";
import ACE_ActorSkillsConfig from "../../apps/skills-config.mjs";

/**
 * An Actor sheet for character type actors.
 * @extends {ACE_ActorSheet}
 */
export class ACE_CharacterSheet extends ACE_ActorSheet {

  // --------------------------------------------

  /**
   * Define default rendering options for the PC sheet.
   * @returns {object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "actor", "character"],
      height: 645
    });
  }

  // --------------------------------------------

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   * @param {object} options
   * @returns {object}  Prepared copy of the actor data ready to be displayed.
   */
  async getData(options={}) {
    return mergeObject(await super.getData(options), {
      isCharacter: true
    });
  }

  /* -------------------------------------------- */

  /**
   * Organize and classify Owned Items for Character sheets
   * @param {object} context  Copy of the actor data being prepared for display. *Will be mutated.*
   * @private
   */
  _prepareItems(context) {

    // Categorize items as inventory, spellbook, features, and classes
    const inventory = {
      weapon: {
        label: "ACE.ItemTypeWeaponPl", items: [], dataset: { type: "weapon" },
        decorations: [
          { label: "unarmed", title: "ACE.Unarmed", symbol: "fa-hand-fist" },
          { label: "dodge", title: "ACE.Dodge", symbol: "fa-person-running" },
          { label: "improvised", title: "ACE.Improvised", symbol: "fa-staff" }
        ]
      },
      equipment: {
        label: "ACE.ItemTypeEquipmentPl", items: [], dataset: { type: "equipment" },
        columns: []
      },
      consumable: { label: "ACE.ItemTypeConsumablePl", items: [], dataset: {type: "consumable"} },
      tool: { label: "ACE.ItemTypeToolPl", items: [], dataset: {type: "tool"} },
      backpack: { label: "ACE.ItemTypeContainerPl", items: [], dataset: {type: "backpack"} },
      loot: { label: "ACE.ItemTypeLootPl", items: [], dataset: {type: "loot"} }
    };

    // Partition items by category
    let {items, skills, backgrounds} = context.items.reduce((obj, item) => {
      const {quantity, uses, recharge, target} = item.system;

      // Item details
      const ctx = context.itemContext[item.id] ??= {};
      ctx.img = item.img || CONST.DEFAULT_TOKEN;
      ctx.isStack = Number.isNumeric(quantity) && (quantity !== 1);

      // Item usage
      ctx.hasUses = uses && (uses.max > 0);
      ctx.isOnCooldown = recharge && !!recharge.value && (recharge.charged === false);
      ctx.isDepleted = ctx.isOnCooldown && (uses.per && (uses.value > 0));
      ctx.hasTarget = !!target && !(["none", ""].includes(target.type));

      // Item toggle state
      this._prepareItemToggleState(item, ctx);

      // Classify items into types
      if (item.type === "background") obj.backgrounds.push(item);
      else if (item.type === "skill") obj.skills.push(item);
      else if (Object.keys(inventory).includes(item.type) ) obj.items.push(item);
      return obj;

    }, { items: [], skills: [], backgrounds: [] });

    // Apply active item filters
    items = this._filterItems(items, this._filters.inventory);
    skills = this._filterItems(skills, this._filters.skills);

    // Organize inventory
    for ( let i of items ) {
      const ctx = context.itemContext[i.id] ??= {};
      i.system.quantity = i.system.quantity || 0;
      i.system.weight = i.system.weight || 0;
      ctx.totalWeight = (i.system.quantity * i.system.weight).toNearest(0.1);
      inventory[i.type].items.push(i);
    }

    // Assign and return
    context.inventoryFilters = true;
    context.inventory = Object.values(inventory);
    context.labels.background = backgrounds[0]?.name;
    context.skills = Object.values(skills).sort((a, b) => {
      return a.name.localeCompare(b.name);
    });

    console.log("A·C·E | ACE_CharacterSheet._prepareItems():", this, context);
  }

  /* -------------------------------------------- */

  /**
   * A helper method to establish the displayed preparation state for an item.
   * @param {ACE_Item} item  Item being prepared for display. *Will be mutated.*
   * @private
   */
  _prepareItemToggleState(item) {
    const isActive = !!item.system.equipped;
    item.toggleClass = isActive ? "active" : "";
    item.toggleTitle = game.i18n.localize(isActive ? "ACE.Equipped" : "ACE.Unequipped");
  }

  // --------------------------------------------
  // Event Listeners and Handlers
  // --------------------------------------------

  /**
   * Activate event listeners using the prepared sheet HTML.
   * @param {jQuery} html   The prepared HTML object ready to be rendered into the DOM.
   */
  activateListeners(html) {
    super.activateListeners(html);
    if ( !this.isEditable ) return;

    // Item State Toggling
    html.find(".item-toggle").click(this._onToggleItem.bind(this));
    html.find("ul.skills-list .config-button").click(this._onSkillsConfig.bind(this));
  }

  // --------------------------------------------

  /**
   * Handle toggling the state of an Owned Item within the Actor.
   * @param {Event} event        The triggering click event.
   * @returns {Promise<ACE_Item>}  Item with the updates applied.
   * @private
   */
  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    const attr = "system.equipped";
    return item.update({[attr]: !foundry.utils.getProperty(item, attr)});
  }

  /**
   * Display the Skills Config panel for the actor.
   * @param {Event} event        The triggering click event.
   * @private
   */
  _onSkillsConfig(event) {
    event.preventDefault();
    const skills = this.actor.items.filter(item => item.type === "skill");
    let app = new ACE_ActorSkillsConfig(this.actor, skills);
    app?.render(true);
  }
}
