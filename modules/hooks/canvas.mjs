import { measureDistances } from "../canvas/_module.mjs";

/* -------------------------------------------- */
/*  Canvas Initialization                       */
/* -------------------------------------------- */

/**
 *
 */
export default function() {
  Hooks.on("canvasInit", function() {
    // Extend Diagonal Measurement
    canvas.grid.diagonalRule = game.settings.get("ACE", "diagonalMovement");
    SquareGrid.prototype.measureDistances = measureDistances;
  });
}
