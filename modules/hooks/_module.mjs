import * as initHook from "./init.mjs";
import * as i18nHook from "./i18n.mjs";
// Import * as readyHook from "./ready.mjs"
import * as canvasHook from "./canvas.mjs";
import * as chatHook from "./chat.mjs";
// Import * as combatHook from "./combat.mjs"
// import * as controlButtonHook from "./getSceneControlButtons.mjs"
// import * as hotbarHook from "./hotbarDrop.mjs"
// import * as actorHook from "./actor.mjs"
// import * as itemHook from "./item.mjs"
// import * as effectHook from "./activeEffects.mjs"
// import * as journalHook from "./journal.mjs"
import * as sidebarHook from "./sidebar.mjs";
// Import * as rolltableHook from "./rolltable.mjs"
// import * as contextHook from "./entryContext.mjs"
import * as tokenHook from "./token.mjs";
// Import * as moduleHook from "./moduleHook.mjs"
import * as setupHook from "./setup.mjs";
// Import * as handlebarsHelpers from "./handlebars.mjs"
// import * as keepId from "./keepId.mjs"

/**
 *
 */
function registerHooks() {
  initHook.default();
  i18nHook.default();
  // ReadyHook.default();
  canvasHook.default();
  chatHook.default();
  // CombatHook.default();
  // controlButtonHook.default();
  // hotbarHook.default();
  // actorHook.default();
  // itemHook.default();
  // EffectHook.default();
  // journalHook.default();
  sidebarHook.default();
  // RolltableHook.default();
  // contextHook.default();
  tokenHook.default();
  // ModuleHook.default();
  setupHook.default();
  // HandlebarsHelpers.default();
  // keepId.default();
}

registerHooks();
