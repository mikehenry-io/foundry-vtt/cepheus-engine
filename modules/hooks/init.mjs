import ACE from "../config.mjs";
import { registerSystemSettings } from "../settings.mjs";

import * as actors from "../actors/_module.mjs";
import * as rolls from "../rolls/_module.mjs";
import * as items from "../items/_module.mjs";
import * as utils from "../utils/_module.mjs";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

/**
 *
 */
export default function() {
  Hooks.once("init", async function() {
    globalThis.ACE = game.ACE = Object.assign(game.system, globalThis.ACE);
    console.log(
      "A·C·E | Initializing the Advanced Cepheus Engine Game System",
      ACE.LOGO.join("\n")
    );

    // Record Configuration Values
    CONFIG.ACE = ACE;
    CONFIG.Actor.documentClass = actors.ACE_Actor;
    CONFIG.Item.documentClass = items.ACE_Item;
    CONFIG.time.roundTime = 6;
    CONFIG.Dice.CheckRoll = rolls.ACE_Check;
    CONFIG.Dice.DamageRoll = rolls.ACE_Damage;

    // Register System Settings
    registerSystemSettings();

    // Patch Core Functions
    // CONFIG.Combat.initiative.formula = "2d6 + @characteristics.dex.mod";
    // Combatant.prototype._getInitiativeFormula = getInitiativeFormula;

    // Register Roll Extensions
    CONFIG.Dice.rolls.push(rolls.ACE_Check);
    CONFIG.Dice.rolls.push(rolls.ACE_Damage);

    // Register Actor sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("ACE", actors.sheets.ACE_CharacterSheet, {
      types: ["character"],
      makeDefault: true,
      label: "ACE.SheetClassCharacter"
    });
    Actors.registerSheet("ACE", actors.sheets.ACE_NPCSheet, {
      types: ["npc"],
      makeDefault: true,
      label: "ACE.SheetClassNPC"
    });
    Actors.registerSheet("ACE", actors.sheets.ACE_UnitSheet, {
      types: ["unit"],
      makeDefault: true,
      label: "ACE.SheetClassUnit"
    });
    Actors.registerSheet("ACE", actors.sheets.ACE_VehicleSheet, {
      types: ["vehicle"],
      makeDefault: true,
      label: "ACE.SheetClassVehicle"
    });
    Actors.registerSheet("ACE", actors.sheets.ACE_WorldSheet, {
      types: ["world"],
      makeDefault: true,
      label: "ACE.SheetClassWorld"
    });

    // Register Item sheet application classes
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("ACE", items.sheets.ACE_EquipmentSheet, {
      types: ["equipment"],
      makeDefault: true,
      label: "ACE.SheetClassEquipment"
    });
    Items.registerSheet("ACE", items.sheets.ACE_SkillSheet, {
      types: ["skill"],
      makeDefault: true,
      label: "ACE.SheetClassSkill"
    });
    Items.registerSheet("ACE", items.sheets.ACE_WeaponSheet, {
      types: ["weapon"],
      makeDefault: true,
      label: "ACE.SheetClassWeapon"
    });

    // Preload Handlebars Templates
    utils.registerHandlebarsHelpers();
    utils.preloadHandlebarsTemplates();
  });
}
