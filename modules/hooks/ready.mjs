/* -------------------------------------------- */
/*  Foundry VTT Ready                           */
/* -------------------------------------------- */

/**
 * Once the entire VTT framework is initialized, check to see if we should perform a data migration
 */
Hooks.once("ready", function() {
  // // Apply custom compendium styles to the SRD rules compendium.
  // const rules = game.packs.get("ACE.rules");
  // rules.apps = [new applications.SRDCompendium(rules)];

  // // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  // Hooks.on("hotbarDrop", (bar, data, slot) => {
  //   if ( ["Item", "ActiveEffect"].includes(data.type) ) {
  //     documents.macro.create5eMacro(data, slot);
  //     return false;
  //   }
  // });

  // Determine whether a system migration is required and feasible
  if ( !game.user.isGM ) return;
  const cv = game.settings.get("ACE", "systemMigrationVersion") || game.world.flags.ACE?.version;
  const totalDocuments = game.actors.size + game.scenes.size + game.items.size;
  if ( !cv && totalDocuments === 0 ) return game.settings.set("ACE", "systemMigrationVersion", game.system.version);
  if ( cv && !isNewerVersion(game.system.flags.needsMigrationVersion, cv) ) return;

  // Perform the migration
  if ( cv && isNewerVersion(game.system.flags.compatibleMigrationVersion, cv) ) {
    ui.notifications.error(
      game.i18n.localize("MIGRATION.ACE_VersionTooOldWarning"),
      { permanent: true }
    );
  }
  migrations.migrateWorld();
});
