/* -------------------------------------------- */
/* Check Roll                                     */
/* -------------------------------------------- */

/**
 * Configuration data for a Check roll.
 *
 * @typedef {object} ACE_CheckConfiguration
 *
 * @property {string[]} [parts=[]]  The dice roll component parts, excluding the initial Check.
 * @property {object} [data={}]     Data that will be used when parsing this roll.
 * @property {Event} [event]        The triggering event for this roll.
 *
 * ## Check Properties
 * @property {boolean} [advantage]     Apply advantage to this roll (unless overridden by modifier keys or dialog)?
 * @property {boolean} [disadvantage]  Apply disadvantage to this roll (unless overridden by modifier keys or dialog)?
 * @property {number|null} [exc_success=6]  The value of the Check result which represents a exceptional success,
 *                                     `null` will prevent exceptional successes.
 * @property {number|null} [exc_failure=-6]  The value of the Check result which represents a exceptional failure,
 *                                     `null` will prevent exceptional failures.
 * @property {number} [targetValue=7]    The value of the Check result which should represent a successful roll.
 *
 * ## Flags
 * @property {boolean} [elvenAccuracy]   Allow Elven Accuracy to modify this roll?
 * @property {boolean} [halflingLucky]   Allow Halfling Luck to modify this roll?
 * @property {boolean} [reliableTalent]  Allow Reliable Talent to modify this roll?
 *
 * ## Roll Configuration Dialog
 * @property {boolean} [fastForward=false]     Should the roll configuration dialog be skipped?
 * @property {boolean} [chooseModifier=false]  If the configuration dialog is shown, should the characteristic modifier
 *                                             be configurable within that interface?
 * @property {string} [template]               The HTML template used to display the roll configuration dialog.
 * @property {string} [title]                  Title of the roll configuration dialog.
 * @property {object} [dialogOptions]          Additional options passed to the roll configuration dialog.
 *
 * ## Chat Message
 * @property {boolean} [chatMessage=true]  Should a chat message be created for this roll?
 * @property {object} [messageData={}]     Additional data which is applied to the created chat message.
 * @property {string} [rollMode]           Value of `CONST.DICE_ROLL_MODES` to apply as default for the chat message.
 * @property {object} [flavor]             Flavor text to use in the created chat message.
 */

/**
 * A standardized helper function for managing core A·C·E Check rolls.
 * Holding SHIFT, ALT, or CTRL when the attack is rolled will "fast-forward".
 * This chooses the default options of a normal attack with no bonus, Advantage, or Disadvantage respectively
 *
 * @param {ACE_CheckConfiguration} configuration  Configuration data for the Check roll.
 * @returns {Promise<ACE_Check|null>}             The evaluated ACE_Check, or null if the workflow was cancelled.
 */
export async function makeCheck({
  parts=[], data={}, event,
  advantage, disadvantage, exc_success=5, exc_failure=-5, targetValue=0,
  fastForward=false, chooseModifier=false, template, title, dialogOptions,
  chatMessage = true, messageData = {}, rollMode, flavor
} = {}) {

  // Handle input arguments
  const formula = ["2d6-7"].concat(parts).join(" + ");
  const { advantageMode, isFF } = _determineAdvantageMode({advantage, disadvantage, fastForward, event});
  const defaultRollMode = rollMode || game.settings.get("core", "rollMode");
  if ( chooseModifier && !isFF ) {
    data.mod = "@mod";
    if ( "characteristicCheckModifier" in data ) data.characteristicCheckModifier = "@characteristicCheckModifier";
  }

  // Construct the D20Roll instance
  const roll = new CONFIG.Dice.CheckRoll(formula, data, {
    flavor: flavor || title,
    advantageMode,
    defaultRollMode,
    rollMode,
    exc_success,
    exc_failure,
    targetValue
  });

  // Prompt a Dialog to further configure the ACE_Check
  if ( !isFF ) {
    const configured = await roll.configureDialog({
      title,
      chooseModifier,
      defaultAction: advantageMode,
      defaultCharacteristic: data?.item?.characteristic || data?.defaultCharacteristic,
      defaultRollMode,
      template
    }, dialogOptions);
    if ( configured === null ) return null;
  } else roll.options.rollMode ??= defaultRollMode;

  // Evaluate the configured roll
  await roll.evaluate({ async: true });

  // Create a Chat Message
  if ( roll && chatMessage ) await roll.toMessage(messageData);

  return roll;
}

/* -------------------------------------------- */

/**
 * Determines whether this Check roll should be fast-forwarded, and whether advantage or disadvantage should be applied
 * @param {object} [config]
 * @param {Event} [config.event]           Event that triggered the roll.
 * @param {boolean} [config.advantage]     Is something granting this roll advantage?
 * @param {boolean} [config.disadvantage]  Is something granting this roll disadvantage?
 * @param {boolean} [config.fastForward]   Should the roll dialog be skipped?
 * @returns {{isFF: boolean, advantageMode: number}}  Whether the roll is fast-forward, and its advantage mode.
 */
function _determineAdvantageMode({
  event, advantage = false, disadvantage = false, fastForward = false
} = {}) {
  const isFF = fastForward || (event && (event.shiftKey || event.altKey || event.ctrlKey || event.metaKey));
  let advantageMode = CONFIG.Dice.CheckRoll.ADV_MODE.NORMAL;
  if ( advantage || event?.altKey ) advantageMode = CONFIG.Dice.CheckRoll.ADV_MODE.ADVANTAGE;
  else if ( disadvantage || event?.ctrlKey || event?.metaKey ) {
    advantageMode = CONFIG.Dice.CheckRoll.ADV_MODE.DISADVANTAGE;
  }
  return {isFF, advantageMode};
}

/* -------------------------------------------- */
/* Damage Roll                                  */
/* -------------------------------------------- */

/**
 * Configuration data for a damage roll.
 *
 * @typedef {object} DamageRollConfiguration
 *
 * @property {string[]} [parts=[]]  The dice roll component parts.
 * @property {object} [data={}]     Data that will be used when parsing this roll.
 * @property {Event} [event]        The triggering event for this roll.
 *
 * ## Exceptional Handling
 * @property {boolean} [allowExceptional=true]  Is this damage roll allowed to be rolled as exceptional?
 * @property {boolean} [exceptional=false]      Apply exceptional to this roll (unless overridden by modifier key or
 *                                              dialog)?
 * @property {number} [exceptionalBonusDice]    A number of bonus damage dice that are added for exceptional hits.
 * @property {number} [exceptionalMultiplier]   Multiplier to use when calculating exceptional damage.
 * @property {boolean} [multiplyNumeric]        Should numeric terms be multiplied when this roll exceptionals?
 * @property {boolean} [powerfulExceptional]    Should the exceptional dice be maximized rather than rolled?
 * @property {string} [exceptionalBonusDamage]  An extra damage term that is applied only on a exceptional hit.
 *
 * ## Roll Configuration Dialog
 * @property {boolean} [fastForward=false]  Should the roll configuration dialog be skipped?
 * @property {string} [template]            The HTML template used to render the roll configuration dialog.
 * @property {string} [title]               Title of the roll configuration dialog.
 * @property {object} [dialogOptions]       Additional options passed to the roll configuration dialog.
 *
 * ## Chat Message
 * @property {boolean} [chatMessage=true]  Should a chat message be created for this roll?
 * @property {object} [messageData={}]     Additional data which is applied to the created chat message.
 * @property {string} [rollMode]           Value of `CONST.DICE_ROLL_MODES` to apply as default for the chat message.
 * @property {string} [flavor]             Flavor text to use in the created chat message.
 */

/**
 * A standardized helper function for managing core ace damage rolls.
 * Holding SHIFT, ALT, or CTRL when the attack is rolled will "fast-forward".
 * This chooses the default options of a normal attack with no bonus, Exceptional, or no bonus respectively
 *
 * @param {DamageRollConfiguration} configuration  Configuration data for the Damage roll.
 * @returns {Promise<DamageRoll|null>}             The evaluated DamageRoll, or null if the workflow was canceled.
 */
export async function damageRoll({
  parts=[], data={}, event,
  allowExceptional=true, exceptional=false, exceptionalBonusDice, exceptionalMultiplier,
  multiplyNumeric, powerfulExceptional, exceptionalBonusDamage,
  fastForward=false, template, title, dialogOptions,
  chatMessage=true, messageData={}, rollMode, flavor
}={}) {

  // Handle input arguments
  const defaultRollMode = rollMode || game.settings.get("core", "rollMode");

  // Construct the DamageRoll instance
  const formula = parts.join(" + ");
  const {isExceptional, isFF} = _determineExceptionalMode({exceptional, fastForward, event});
  const roll = new CONFIG.Dice.ACE_DamageRoll(formula, data, {
    flavor: flavor || title,
    rollMode,
    exceptional: isFF ? isExceptional : false,
    exceptionalBonusDice,
    exceptionalMultiplier,
    exceptionalBonusDamage,
    multiplyNumeric: multiplyNumeric ?? game.settings.get("ace", "exceptionalDamageModifiers"),
    powerfulExceptional: powerfulExceptional ?? game.settings.get("ace", "exceptionalDamageMaxDice")
  });

  // Prompt a Dialog to further configure the DamageRoll
  if ( !isFF ) {
    const configured = await roll.configureDialog({
      title,
      defaultRollMode: defaultRollMode,
      defaultExceptional: isExceptional,
      template,
      allowExceptional
    }, dialogOptions);
    if ( configured === null ) return null;
  }

  // Evaluate the configured roll
  await roll.evaluate({async: true});

  // Create a Chat Message
  if ( roll && chatMessage ) await roll.toMessage(messageData);
  return roll;
}

/* -------------------------------------------- */

/**
 * Determines whether this Check roll should be fast-forwarded, and whether advantage or disadvantage should be applied
 * @param {object} [config]
 * @param {Event} [config.event]          Event that triggered the roll.
 * @param {boolean} [config.exceptional]     Is this roll treated as exceptional by default?
 * @param {boolean} [config.fastForward]  Should the roll dialog be skipped?
 * @returns {{isFF: boolean, isExceptional: boolean}}  Whether the roll is fast-forward, and/or is an exceptional hit
 */
function _determineExceptionalMode({event, exceptional=false, fastForward=false}={}) {
  const isFF = fastForward || (event && (event.shiftKey || event.altKey || event.ctrlKey || event.metaKey));
  if ( event?.altKey ) exceptional = true;
  return {isFF, isExceptional: exceptional};
}
