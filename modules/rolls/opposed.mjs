import { default as ACE_Check } from "./check.mjs";

/**
 * A type of Roll specific to a 2d6-based check in the A·C·E system.
 * @param {string} formula                       The string formula to parse
 * @param {object} data                          The data object against which to parse attributes within the formula
 * @param {object} [options={}]                  Extra optional arguments which describe or modify the Check
 * @param {number} [options.advantageMode]       What advantage modifier to apply to the roll (none, advantage,
 *                                               disadvantage)
 * @param {number} [options.exc_success=5]       The value of the check result which represents an exceptional success
 * @param {number} [options.exc_failure=-5]      The value of the check result which represents an exceptional failure
 * @param {(number)} [options.targetValue=7]     Assign a target value against which the result of this roll should be
 *                                               compared
 */
export default class ACE_OpposedCheck {
  constructor(formula, data, options) {
    this.data = {
      messageId: data.messageId,
      attackerMessageId: data.attackerMessageId,
      defenderMessageId: data.defenderMessageId,
      resultMessageId: data.resultMessageId,
      targetSpeakerData: data.targetSpeakerData,
      options: data.options || {},
      unopposed: data.unopposed
    };
  }
}
