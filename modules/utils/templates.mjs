/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @returns {Promise}
 */
export async function preloadHandlebarsTemplates() {
  const partials = [
    // Shared Partials
    "systems/ACE/templates/actors/parts/character-bio.hbs",
    "systems/ACE/templates/actors/parts/character-body.hbs",
    "systems/ACE/templates/actors/parts/character-der.hbs",
    // "systems/ACE/templates/actors/parts/character-inv.hbs",
    "systems/ACE/templates/actors/parts/character-nav.hbs",
    "systems/ACE/templates/actors/parts/character-sum.hbs",
    "systems/ACE/templates/actors/parts/character-ufc.hbs",
    "systems/ACE/templates/actors/parts/active-effects.hbs",

    // Actor Sheet Partials
    "systems/ACE/templates/actors/parts/actor-traits.hbs",
    "systems/ACE/templates/actors/parts/actor-inventory.hbs",
    // "systems/ACE/templates/actors/parts/actor-features.hbs",
    // "systems/ACE/templates/actors/parts/actor-spellbook.hbs",
    // "systems/ACE/templates/actors/parts/actor-warnings.hbs",

    // Item Sheet Partials
    "systems/ACE/templates/items/parts/item-action.hbs",
    "systems/ACE/templates/items/parts/item-activation.hbs",
    "systems/ACE/templates/items/parts/item-description.hbs",
    "systems/ACE/templates/items/parts/item-mountable.hbs"
  ];

  const paths = partials.reduce((acc, path) => ({
    ...acc,
    // [path.replace(".hbs", ".html")]: path,
    [`ACE.${path.split("/").pop().replace(".hbs", "")}`]: path
  }), {});

  // Const paths = {};
  // for ( const path of partials ) {
  //   // Paths[path.replace(".hbs", ".html")] = path;
  //   paths[`ACE.${path.split("/").pop().replace(".hbs", "")}`] = path;
  // }

  console.log("A·C·E | preloadHandlebarsTemplates():", paths);

  return loadTemplates(paths);
}

/* -------------------------------------------- */

/**
 * Register custom Handlebars helpers used by ACE.
 */
export function registerHandlebarsHelpers() {
  Handlebars.registerHelper("tetratrigesimal", function(decimal) {
    return ACE.tetratrigesimal[parseInt(decimal)];
  });
}
