import { preLocalize } from "./utils/localization.mjs";

// Namespace Configuration Values
export const ACE = {};

// ASCII Artwork
ACE.LOGO = [
  "┌─────────────────────────────────────────────────────────────┐",
  "│                ╔═══════════════════════════╗                │",
  "│                ║ ░░░█▀█░░░░░█▀▀░░░░░█▀▀░░░ ║                │",
  "│                ║ ░░░█▀█░░▀░░█░░░░▀░░█▀▀░░░ ║                │",
  "│                ║ ░░░▀░▀░░░░░▀▀▀░░░░░▀▀▀░░░ ║                │",
  "│                ╚═══════════════════════════╝                │",
  "│        A D V A N C E D   C E P H E U S   E N G I N E        │",
  "└─────────────────────────────────────────────────────────────┘"
];

/**
 * The set of Characteristic Scores used within the system.
 * @enum {string}
 */
ACE.characteristics = {
  str: "ACE.CharacteristicStr",
  dex: "ACE.CharacteristicDex",
  end: "ACE.CharacteristicEnd",
  int: "ACE.CharacteristicInt",
  edu: "ACE.CharacteristicEdu",
  soc: "ACE.CharacteristicSoc"
};
preLocalize("characteristics");

/**
 * Localized abbreviations for Characteristic Scores.
 * @enum {string}
 */
ACE.characteristicAbbreviations = {
  str: "ACE.CharacteristicStrAbbr",
  dex: "ACE.CharacteristicDexAbbr",
  end: "ACE.CharacteristicEndAbbr",
  int: "ACE.CharacteristicIntAbbr",
  edu: "ACE.CharacteristicEduAbbr",
  soc: "ACE.CharacteristicSocAbbr"
};
preLocalize("characteristicAbbreviations");

/* -------------------------------------------- */

/**
 * The various lengths of time over which effects can occur.
 * @enum {string}
 */
ACE.timePeriods = {
  inst: "ACE.TimeInst",
  turn: "ACE.TimeTurn",
  round: "ACE.TimeRound",
  minute: "ACE.TimeMinute",
  hour: "ACE.TimeHour",
  day: "ACE.TimeDay",
  month: "ACE.TimeMonth",
  year: "ACE.TimeYear",
  perm: "ACE.TimePerm",
  special: "ACE.TimeSpecial"
};
preLocalize("timePeriods");

/* -------------------------------------------- */

/**
 * Various ways in which an item or characteristic can be activated.
 * @enum {string}
 */
ACE.characteristicActivationTypes = {
  none: "ACE.None",
  bonus: "ACE.ActionBonus",
  major: "ACE.ActionMajor",
  minor: "ACE.ActionMinor",
  reaction: "ACE.Reaction",
  minute: ACE.timePeriods.minute,
  hour: ACE.timePeriods.hour,
  day: ACE.timePeriods.day,
  special: ACE.timePeriods.special,
  legendary: "ACE.LegendaryAction",
  lair: "ACE.LairAction",
  crew: "ACE.CrewAction"
};
preLocalize("characteristicActivationTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Different things that an characteristic can consume upon use.
 * @enum {string}
 */
ACE.characteristicConsumptionTypes = {
  ammunition: "ACE.ConsumeAmmunition",
  attribute: "ACE.ConsumeAttribute",
  hitDice: "ACE.ConsumeHitDice",
  material: "ACE.ConsumeMaterial",
  charges: "ACE.ConsumeCharges"
};
preLocalize("characteristicConsumptionTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Individual sizes.
 * @enum {string}
 */
ACE.actorSizes = {
  med: "ACE.SizeMedium",
  fin: "ACE.SizeFine",
  dim: "ACE.SizeDiminutive",
  tny: "ACE.SizeTiny",
  sml: "ACE.SizeSmall",
  lrg: "ACE.SizeLarge",
  hug: "ACE.SizeHuge",
  grg: "ACE.SizeGargantuan",
  col: "ACE.SizeColossal"
};
preLocalize("actorSizes");

/**
 * Default token image size for the values of `ACE.actorSizes`.
 * @enum {number}
 */
ACE.tokenSizes = {
  med: 1,
  fin: 0.125,
  dim: 0.25,
  tny: 0.5,
  sml: 0.75,
  lrg: 2,
  hug: 4,
  grg: 8,
  col: 16
};

/* -------------------------------------------- */

/**
 * Classification types for item action types.
 * @enum {string}
 */
ACE.itemActionTypes = {
  melee: "ACE.ActionMelee",
  ranged: "ACE.ActionRanged",
  healing: "ACE.ActionHeal",
  characteristic: "ACE.ActionCharacteristic",
  utility: "ACE.ActionUtility",
  other: "ACE.ActionOther"
};
preLocalize("itemActionTypes");

/* -------------------------------------------- */

/**
 * Specific equipment types that modify base AC.
 * @enum {string}
 */
ACE.armorTypes = {
  light: "ACE.EquipmentLight",
  medium: "ACE.EquipmentMedium",
  heavy: "ACE.EquipmentHeavy",
  natural: "ACE.EquipmentNatural",
  shield: "ACE.EquipmentShield"
};
preLocalize("armorTypes");

/* -------------------------------------------- */

/**
 * Equipment types that aren't armor.
 * @enum {string}
 */
ACE.miscEquipmentTypes = {
  clothing: "ACE.EquipmentClothing",
  trinket: "ACE.EquipmentTrinket"
};
preLocalize("miscEquipmentTypes", { sort: true });

/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can be worn by the character.
 * @enum {string}
 */
ACE.equipmentTypes = {
  ...ACE.miscEquipmentTypes,
  ...ACE.armorTypes
};
preLocalize("equipmentTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system.
 * @enum {string}
 */
ACE.consumableTypes = {
  ammunition: "ACE.ConsumableAmmunition",
  poison: "ACE.ConsumablePoison",
  food: "ACE.ConsumableFood",
  trinket: "ACE.ConsumableTrinket"
};
preLocalize("consumableTypes", { sort: true });

/* -------------------------------------------- */
/*  Damage Types                                */
/* -------------------------------------------- */

/**
 * Types of damage that are considered physical.
 * @enum {string}
 */
ACE.physicalDamageTypes = {
  bludgeoning: "ACE.DamageBludgeoning",
  piercing: "ACE.DamagePiercing",
  slashing: "ACE.DamageSlashing"
};
preLocalize("physicalDamageTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Types of damage the can be caused by abilities.
 * @enum {string}
 */
ACE.damageTypes = {
  ...ACE.physicalDamageTypes,
  acid: "ACE.DamageAcid",
  ballistic: "ACE.DamageBallistic",
  cold: "ACE.DamageCold",
  energy: "ACE.DamageEnergy",
  fire: "ACE.DamageFire",
  force: "ACE.DamageForce",
  lightning: "ACE.DamageLightning",
  necrotic: "ACE.DamageNecrotic",
  poison: "ACE.DamagePoison",
  psychic: "ACE.DamagePsychic",
  radiant: "ACE.DamageRadiant",
  thunder: "ACE.DamageThunder"
};
preLocalize("damageTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Types of damage to which an actor can possess resistance, immunity, or vulnerability.
 * @enum {string}
 * @deprecated
 */
ACE.damageResistanceTypes = {
  ...ACE.damageTypes,
  physical: "ACE.DamagePhysical"
};
preLocalize("damageResistanceTypes", { sort: true });

/* -------------------------------------------- */
/*  Movement                                    */
/* -------------------------------------------- */

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @enum {string}
 */
ACE.movementTypes = {
  burrow: "ACE.MovementBurrow",
  climb: "ACE.MovementClimb",
  fly: "ACE.MovementFly",
  swim: "ACE.MovementSwim",
  walk: "ACE.MovementWalk"
};
preLocalize("movementTypes", { sort: true });

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the metric units of meter and kilometer.
 * @enum {string}
 */
ACE.movementUnits = {
  m: "ACE.DistM",
  km: "ACE.DistKm",
  ft: "ACE.DistFt",
  mi: "ACE.DistMi"
};
preLocalize("movementUnits");

/**
 * The valid units of measure for the range of an action or effect.
 * This object automatically includes the movement units from `ACE.movementUnits`.
 * @enum {string}
 */
ACE.distanceUnits = {
  none: "ACE.None",
  self: "ACE.DistSelf",
  touch: "ACE.DistTouch",
  spec: "ACE.Special",
  any: "ACE.DistAny",
  ...ACE.movementUnits
};
preLocalize("distanceUnits");

/* -------------------------------------------- */

/**
 * The types of single or area targets which can be applied to abilities.
 * @enum {string}
 */
ACE.targetTypes = {
  none: "ACE.None",
  self: "ACE.TargetSelf",
  individual: "ACE.TargetIndividual",
  ally: "ACE.TargetAlly",
  enemy: "ACE.TargetEnemy",
  object: "ACE.TargetObject",
  space: "ACE.TargetSpace",
  radius: "ACE.TargetRadius",
  cone: "ACE.TargetCone",
  cube: "ACE.TargetCube",
  cylinder: "ACE.TargetCylinder",
  line: "ACE.TargetLine",
  sphere: "ACE.TargetSphere",
  square: "ACE.TargetSquare",
  wall: "ACE.TargetWall"
};
preLocalize("targetTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Mapping between `ACE.targetTypes` and `MeasuredTemplate` shape types to define
 * which templates are produced by which area of effect target type.
 * @enum {string}
 */
ACE.areaTargetTypes = {
  cone: "cone",
  cube: "rect",
  cylinder: "circle",
  line: "ray",
  radius: "circle",
  sphere: "circle",
  square: "rect",
  wall: "ray"
};

/* -------------------------------------------- */

/**
 * The set of possible sensory perception types which an Actor may have.
 * @enum {string}
 */
ACE.senses = {
  blindsight: "ACE.SenseBlindsight",
  darkvision: "ACE.SenseDarkvision",
  low_lightvision: "ACE.SenseLowLightVision"
};
preLocalize("senses", { sort: true });

/* -------------------------------------------- */
/*  Weapon Details                              */
/* -------------------------------------------- */

/**
 * The set of types which a weapon item can take.
 * @enum {string}
 */
ACE.weaponTypes = {
  heavy: "ACE.WeaponHeavy",
  improv: "ACE.WeaponImprovised",
  melee: "ACE.WeaponMelee",
  natural: "ACE.WeaponNatural",
  ranged: "ACE.WeaponRanged"
};
preLocalize("weaponTypes");

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object. In cases where multiple pieces
 * of cover are in play, we take the highest value.
 * @enum {string}
 */
ACE.cover = {
  0: "ACE.None",
  .5: "ACE.CoverHalf",
  .75: "ACE.CoverThreeQuarters",
  1: "ACE.CoverTotal"
};
preLocalize("cover");

/**
 * Locomotion constants for multi-pedal creatres and characters.
 * @enum {string}
 */
ACE.locoNames = {
  bipedal: "ACE.LocoBiped",
  quadrupedal: "ACE.LocoQuadruped",
  hexapedal: "ACE.LocoHexaped"
};
preLocalize("locoNames");

ACE.locoTypes = {
  ...ACE.locoNames,
  other: "ACE.Other"
};
preLocalize("locoTypes");

/**
 * Locomotion constants for multi-pedal creatres and characters.
 * @enum {object{string, number}}
 */
ACE.locoWeights = {
  bipedal: { fin: 0.125, dim: 0.25, tny: 0.50, sml: 0.75, med: 1.0, lrg: 2, hug: 4, grg: 8, col: 16 },
  quadrupedal: { fin: 0.250, dim: 0.50, tny: 0.75, sml: 1.00, med: 1.5, lrg: 3, hug: 6, grg: 12, col: 24 },
  hexapedal: { fin: 0.375, dim: 0.75, tny: 1.00, sml: 1.25, med: 2.0, lrg: 4, hug: 8, grg: 16, col: 32 }
};

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object. In cases where multiple pieces
 * of cover are in play, we take the highest value.
 * @enum {string}
 */
ACE.cover = {
  0: "ACE.None",
  .5: "ACE.CoverHalf",
  .75: "ACE.CoverThreeQuarters",
  1: "ACE.CoverTotal"
};
preLocalize("cover");

/* -------------------------------------------- */

/**
 * A selection of actor attributes that can be tracked on token resource bars.
 * @type {string[]}
 */
ACE.trackableAttributes = [
  "attributes.init.value",
  "attributes.movement",
  "skills.*.passive",
  "characteristics.*.value"
];

/* -------------------------------------------- */

/**
 * A selection of actor and item attributes that are valid targets for item resource consumption.
 * @type {string[]}
 */
ACE.consumableResources = [
  "item.quantity",
  "item.weight",
  "currency",
  "characteristics.*.value",
  "attributes.movement",
  "item.armor.value",
  "item.target",
  "item.range"
];

/* -------------------------------------------- */

/**
 * Conditions that can effect an actor.
 * @enum {string}
 */
ACE.conditionTypes = {
  crouched: "ACE.ConCrouched",
  off_guard: "ACE.ConOffGuard",
  prone: "ACE.ConProne",
  surprised: "ACE.ConSurprised",

  blinded: "ACE.ConBlinded",
  charmed: "ACE.ConCharmed",
  deafened: "ACE.ConDeafened",
  diseased: "ACE.ConDiseased",
  exhaustion: "ACE.ConExhaustion",
  frightened: "ACE.ConFrightened",
  grappled: "ACE.ConGrappled",
  incapacitated: "ACE.ConIncapacitated",
  invisible: "ACE.ConInvisible",
  paralyzed: "ACE.ConParalyzed",
  petrified: "ACE.ConPetrified",
  poisoned: "ACE.ConPoisoned",
  restrained: "ACE.ConRestrained",
  stunned: "ACE.ConStunned",
  unconscious: "ACE.ConUnconscious"
};
preLocalize("conditionTypes", { sort: true });

/**
 * An array of tetratrigesimal digits for O[1]/O[n] conversion.
 *
 * @type {string[]}
 */
ACE.tetratrigesimal = [
  "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
  "a", "b", "c", "d", "e", "f", "g", "h", "j", "k",
  "l", "m", "n", "p", "q", "r", "s", "t", "u", "v",
  "w", "x", "y", "z"
];

export default ACE;
