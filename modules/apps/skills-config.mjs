/**
 * A simple form to set save throw configuration for a given skills score.
 *
 * @param {ACE_Actor} actor             The Actor instance being displayed within the sheet.
 * @param {ApplicationOptions} options  Additional application configuration options.
 * @param {string} skills             The skills key as defined in CONFIG.ACE.abilities.
 */
export default class ACE_ActorSkillsConfig extends DocumentSheet {
  constructor(actor, options, skills) {
    super(actor, options);
    this._skills = skills;
  }

  /* -------------------------------------------- */

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["ACE"],
      template: "systems/ACE/templates/apps/skills-config.hbs",
      width: 500,
      height: "auto"
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get title() {
    return `Skills Configure: ${this.document.name}`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const actor = this.object.toObject();
    return {
      actor: actor,
      skills: this._skills
    };
  }
}
