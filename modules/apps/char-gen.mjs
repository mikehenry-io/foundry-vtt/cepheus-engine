
/**
 * This class is the center of character generation through the chat prompts (started with /char)
 * Each function usually corresponds with a specific action/button click, processing and rendering
 * a new card in response.
 */
export default class ACE_CharacterCreator {

  static start() {
    game.ACE.creator = new this()
  }

}
