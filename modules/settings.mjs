/**
 *
 */
export function registerSystemSettings() {

  // Diagonal Movement Rule
  game.settings.register("ACE", "diagonalMovement", {
    name: "SETTINGS.ACE_DiagN",
    hint: "SETTINGS.ACE_DiagL",
    scope: "world",
    config: true,
    type: String,
    choices: {
      STD: "SETTINGS.ACE_DiagStd",
      ALT: "SETTINGS.ACE_DiagAlt",
      EUC: "SETTINGS.ACE_DiagEuc"
    },
    default: "STD",
    onChange: rule => canvas.grid.diagonalRule = rule
  });

  // Collapse Item Cards (by default)
  game.settings.register("ACE", "autoCollapseItemCards", {
    name: "SETTINGS.ACE_AutoCollapseCardN",
    hint: "SETTINGS.ACE_AutoCollapseCardL",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: s => {
      ui.chat.render();
    }
  });
}
