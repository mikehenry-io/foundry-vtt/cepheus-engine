import SystemDataModel from "../../abstract.mjs";

export default class ACE_CommonTemplate extends
  SystemDataModel.mixin(ACE_CurrencyTemplate)
{

  /** @inheritdoc */
  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    // ACE_CommonTemplate.#migrateMovementData(source);
  }
}
