import ACE_CharacterData from "./character.mjs";
// Import ACE_UnitData from "./unit.mjs";
// import ACE_NPCData from "./npc.mjs";
// import ACE_VehicleData from "./vehicle.mjs";

export {
  ACE_CharacterData
  // ACE_UnitData,
  // ACE_NPCData,
  // ACE_VehicleData
};

// Export { default as AttributesFields } from "./templates/attributes.mjs";
// export { default as CommonTemplate } from "./templates/common.mjs";
// export { default as CreatureTemplate } from "./templates/creature.mjs";
// export { default as DetailsFields } from "./templates/details.mjs";
// export { default as TraitsFields } from "./templates/traits.mjs";

export const config = {
  character: ACE_CharacterData
  // Unit: ACE_UnitData,
  // npc: ACE_NPCData,
  // vehicle: ACE_VehicleData
};
