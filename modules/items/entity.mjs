import * as rolls from "../rolls/_module.mjs";

/**
 * Override and extend the basic Item implementation.
 * @extends {Item}
 */
export class ACE_Item extends Item {

  /* -------------------------------------------- */
  /*  Item Properties                             */
  /* -------------------------------------------- */

  /**
   * Which characteristic score modifier is used by this item?
   * @type {string|null}
   */
  get characteristicMod() {
    if ( !("characteristic" in this.system) ) return null;

    // Case 1 - defined directly by the item
    if ( this.system.characteristic ) return this.system.characteristic;

    // Case 2 - inferred from a parent actor
    if ( this.actor ) {
      const characteristics = this.actor.system.characteristics;

      // Special rules per item type
      switch ( this.type ) {
        case "tool":
          return "int";
        case "weapon":
          // Finesse weapons - Str or Dex (PHB pg. 147)
          if ( this.system.properties.fin === true ) {
            return characteristics.dex.mod >= characteristics.str.mod ? "dex" : "str";
          }
          // Ranged weapons - Dex (PH p.194)
          if ( ["simpleR", "martialR"].includes(this.system.weaponType) ) return "dex";
          break;
      }

      // If a specific attack type is defined
      if ( this.hasAttack ) return {
        melee: "str",
        ranged: "dex"
      }[this.system.actionType];
    }

    // Case 3 - unknown
    return null;
  }

  /**
   * Does the Item implement an attack roll as part of its usage?
   * @type {boolean}
   */
  get hasAttack() {
    return ["melee", "ranged"].includes(this.system.actionType);
  }

  /* -------------------------------------------- */

  /**
   * Does the Item implement a damage roll as part of its usage?
   * @type {boolean}
   */
  get hasDamage() {
    return !!(this.system.damage && this.system.damage.parts?.length);
  }

  /* -------------------------------------------- */

  /**
   * Does the Item have a target?
   * @type {boolean}
   */
  get hasTarget() {
    const target = this.system.target;
    return target && !["none", ""].includes(target.type);
  }

  /* -------------------------------------------- */

  /**
   * Does the Item have an area of effect target?
   * @type {boolean}
   */
  get hasAreaTarget() {
    const target = this.system.target;
    return target && (target.type in CONFIG.ACE.areaTargetTypes);
  }

  /* -------------------------------------------- */

  /**
   * Is this Item limited in its characteristic to be used by charges or by recharge?
   * @type {boolean}
   */
  get hasLimitedUses() {
    let chg = this.system.recharge || {};
    let uses = this.system.uses || {};
    return !!chg.value || (uses.per && (uses.max > 0));
  }

  /* -------------------------------------------- */

  /**
   * Is this item any of the armor subtypes?
   * @type {boolean}
   */
  get isArmor() {
    return this.system.armor?.type in CONFIG.ACE.armorTypes;
  }

  /* -------------------------------------------- */

  /**
   * Should this item's active effects be suppressed.
   * @type {boolean}
   */
  get areEffectsSuppressed() {
    const requireEquipped = (this.type !== "consumable")
      || ["rod", "trinket", "wand"].includes(this.system.consumableType);

    return requireEquipped && (this.system.equipped === false);
  }

  /* -------------------------------------------- */
  /*  Data Preparation                            */
  /* -------------------------------------------- */

  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareDerivedData() {
    super.prepareDerivedData();
    this.labels = {};

    // Specialized preparation per Item type
    switch (this.type) {
      case "equipment":
        this._prepareEquipment(); break;
      case "trait":
        this._prepareTrait(); break;
      case "fx":
        this._prepareFX(); break;
    }

    // Activated Items
    this._prepareActivation();
    this._prepareAction();

    // Un-owned items can have their final preparation done here, otherwise this needs to happen in the owning Actor
    if (!this.isOwned) this.prepareFinalAttributes();
  }


  /* -------------------------------------------- */

  /**
   * Prepare derived data for activated items and define labels.
   * @protected
   */
  _prepareActivation() {
    if (!("activation" in this.system)) return;
    const C = CONFIG.ACE;

    // Characteristic Activation Label
    const act = this.system.activation ?? {};
    this.labels.activation = [act.cost, C.characteristicActivationTypes[act.type]].filterJoin(" ");

    // Const act_cost = act.cost;
    // const act_type = `${C.characteristicActivationTypes[act.type]}${act_cost > 1 ? "Pl" : ""}`;
    // this.labels.activation = [act_cost, game.i18n.localize(act_type)].filterJoin(" ");
    // Console.log("A·C·E | _prepareActivation():", act.cost, act.type, this.labels.activation);

    // Target Label
    let tgt = this.system.target ?? {};
    if (["none", "touch", "self"].includes(tgt.units)) tgt.value = null;
    if (["none", "self"].includes(tgt.type)) {
      tgt.value = null;
      tgt.units = null;
    }
    this.labels.target = [tgt.value, C.distanceUnits[tgt.units], C.targetTypes[tgt.type]].filterJoin(" ");

    // Range Label
    let rng = this.system.range ?? {};
    if (["none", "touch", "self"].includes(rng.units)) {
      rng.value = null;
      rng.long = null;
    }
    this.labels.range = [rng.value, rng.long ? "/ ${rng.long}" : null, C.distanceUnits[rng.units]].filterJoin(" ");

    // Duration Label
    let dur = this.system.duration ?? {};
    if (["inst", "perm"].includes(dur.units)) dur.value = null;
    this.labels.duration = [dur.value, C.timePeriods[dur.units]].filterJoin(" ");

    // Recharge Label
    let chg = this.system.recharge ?? {};
    const chgSuffix = `${chg.value}${parseInt(chg.value) < 6 ? "+" : ""}`;
    this.labels.recharge = `${game.i18n.localize("ACE.Recharge")} [${chgSuffix}]`;
  }

  /**
   * Prepare derived data and labels for items which have an action which deals damage.
   * @protected
   */
  _prepareAction() {
    if (!("actionType" in this.system)) return;
    let damage = this.system.damage || {};
    if (damage.parts) {
      const types = CONFIG.ACE.damageTypes;

      // Console.log("A·C·E | _prepareAction():", damage);

      this.labels.damage = damage.parts.map(d => d.roll).join(" + ").replace(/\+ -/g, "- ");
      this.labels.damageTypes = damage.parts.map(d => types[d.type]).join(", ");
    }
  }

  /**
   * Prepare derived data for an equipment-type item and define labels.
   * @protected
   */
  _prepareEquipment() {
    this.labels.armor = this.system.armor.value ? `${this.system.armor.value} ${game.i18n.localize("ACE.ArmorValue")}` : "";
  }

  /* -------------------------------------------- */

  /**
   * Compute item attributes which might depend on prepared actor data. If this item is embedded this method will
   * be called after the actor's data is prepared.
   * Otherwise, it will be called at the end of `ACE_Item#prepareDerivedData`.
   */
  prepareFinalAttributes() {

    // Action usage
    if ( "actionType" in this.system ) {
      this.labels.characteristicCheck = game.i18n.format("ACE.CharacteristicPromptTitle", {
        characteristic: CONFIG.ACE.characteristics[this.system.characteristic]
      });

      // Saving throws
      // this.getSaveDC();

      // To Hit
      this.getAttackToHit();

      // Limited Uses
      this.prepareMaxUses();

      // Damage Label
      this.getDerivedDamageLabel();
    }
  }


  /* -------------------------------------------- */

  /**
   * Populate a label with the compiled and simplified damage formula based on owned item
   * actor data. This is only used for display purposes and is not related to `ACE_Item#rollDamage`.
   * @returns {{damageType: string, formula: string, label: string}[]}
   */
  getDerivedDamageLabel() {
    if ( !this.hasDamage || !this.isOwned ) return [];
    const rollData = this.getRollData();
    const damageLabels = {
      ...CONFIG.ACE.damageTypes, ...CONFIG.ACE.healingTypes
    };
    const derivedDamage = this.system.damage?.parts?.map(damagePart => {
      let formula;
      try {
        const roll = new Roll(damagePart.roll, rollData);
        formula = rolls.simplifyRollFormula(roll.formula, { preserveFlavor: true });
      }
      catch(err) {
        console.warn(`Unable to simplify formula for ${this.name}: ${err}`);
      }
      const damageType = damagePart.type;
      return { formula, damageType, label: `${formula} ${damageLabels[damageType] ?? ""}` };
    });
    return this.labels.derivedDamage = derivedDamage;
  }

  /* -------------------------------------------- */

  /**
   * Update a label to the Item detailing its total to hit bonus from the following sources:
   * - item document's innate attack bonus
   * - item's actor's proficiency bonus if applicable
   * - item's actor's global bonuses to the given item type
   * - item's ammunition if applicable
   * @returns {{rollData: object, parts: string[]}|null}  Data used in the item's Attack roll.
   */
  getAttackToHit() {
    if ( !this.hasAttack ) return null;
    const rollData = this.getRollData();
    const parts = [];

    // Include the item's innate attack bonus as the initial value and label
    const ab = this.system.attackBonus;
    if ( ab ) {
      parts.push(ab);
      this.labels.toHit = !/^[+-]/.test(ab) ? `+ ${ab}` : ab;
    }

    // Take no further action for un-owned items
    if ( !this.isOwned ) return {rollData, parts};

    // Ability score modifier
    parts.push("@mod");

    // Add proficiency bonus if an explicit proficiency flag is present or for non-item features
    if ( !["weapon", "consumable"].includes(this.type) || this.system.proficient ) {
      parts.push("@prof");
      if ( this.system.prof?.hasProficiency ) rollData.prof = this.system.prof.term;
    }

    // Actor-level global bonus to attack rolls
    const actorBonus = this.actor.system.bonuses?.[this.system.actionType] || {};
    if ( actorBonus.attack ) parts.push(actorBonus.attack);

    // One-time bonus provided by consumed ammunition
    if ( (this.system.consume?.type === "ammunition") && this.actor.items ) {
      const ammoItem = this.actor.items.get(this.system.consume.target);
      if ( ammoItem ) {
        const ammoItemQuantity = ammoItem.system.quantity;
        const ammoCanBeConsumed = ammoItemQuantity && (ammoItemQuantity - (this.system.consume.amount ?? 0) >= 0);
        const ammoItemAttackBonus = ammoItem.system.attackBonus;
        const ammoIsTypeConsumable = (ammoItem.type === "consumable") && (ammoItem.system.consumableType === "ammo");
        if ( ammoCanBeConsumed && ammoItemAttackBonus && ammoIsTypeConsumable ) {
          parts.push("@ammunition");
          rollData.ammunition = ammoItemAttackBonus;
        }
      }
    }

    // Condense the resulting attack bonus formula into a simplified label
    const r = new Roll(parts.join("+"), rollData);
    const formula = rolls.simplifyRollFormula(r.formula) || "0";
    this.labels.toHit = !/^[+-]/.test(formula) ? `+ ${formula}` : formula;
    return {rollData, parts};
  }

  /* -------------------------------------------- */

  /**
   * Retrieve an item's exceptional hit threshold. Uses the smallest value from among the following sources:
   * - item document
   * - item document's actor (if it has one)
   * - the constant '20'
   * @returns {number|null}  The minimum value that must be rolled to be considered a exceptional hit.
   */
  getExceptionalThreshold() {
    const actorFlags = this.actor.flags.ACE || {};
    if ( !this.hasAttack ) return null;
    let actorThreshold = null;
    if ( this.type === "weapon" ) actorThreshold = actorFlags.weaponExceptionalThreshold;
    else if ( this.type === "spell" ) actorThreshold = actorFlags.spellExceptionalThreshold;
    return Math.min(this.system.exceptional?.threshold ?? 6, actorThreshold ?? 6);
  }

  /* -------------------------------------------- */

  /**
   * Populates the max uses of an item.
   * If the item is an owned item and the `max` is not numeric, calculate based on actor data.
   */
  prepareMaxUses() {
    const uses = this.system.uses;
    if ( !uses?.max ) return;
    let max = uses.max;
    if ( this.isOwned && !Number.isNumeric(max) ) {
      const property = game.i18n.localize("ACE.UsesMax");
      try {
        const rollData = this.actor.getRollData({ deterministic: true });
        max = Roll.safeEval(this.replaceFormulaData(max, rollData, { property }));
      } catch(e) {
        const message = game.i18n.format("ACE.FormulaMalformedError", { property, name: this.name });
        this.actor._preparationWarnings.push({ message, link: this.uuid, type: "error" });
        console.error(message, e);
        return;
      }
    }
    uses.max = Number(max);
  }

  /* -------------------------------------------- */

  /**
   * Replace referenced data attributes in the roll formula with values from the provided data.
   * If the attribute is not found in the provided data, display a warning on the actor.
   * @param {string} formula           The original formula within which to replace.
   * @param {object} data              The data object which provides replacements.
   * @param {object} options
   * @param {string} options.property  Name of the property to which this formula belongs.
   * @returns {string}                 Formula with replaced data.
   */
  replaceFormulaData(formula, data, { property }) {
    const dataRgx = new RegExp(/@([a-z.0-9_-]+)/gi);
    const missingReferences = new Set();
    formula = formula.replace(dataRgx, (match, term) => {
      let value = foundry.utils.getProperty(data, term);
      if ( value == null ) {
        missingReferences.add(match);
        return "0";
      }
      return String(value).trim();
    });
    if ( (missingReferences.size > 0) && this.actor ) {
      const listFormatter = new Intl.ListFormat(game.i18n.lang, { style: "long", type: "conjunction" });
      const message = game.i18n.format("ACE.FormulaMissingReferenceWarn", {
        property, name: this.name, references: listFormatter.format(missingReferences)
      });
      this.actor._preparationWarnings.push({ message, link: this.uuid, type: "warning" });
    }
    return formula;
  }

  /* -------------------------------------------- */

  /**
   * Trigger an item usage, optionally creating a chat message with followup actions.
   * @param {ItemUseConfiguration} [config]      Initial configuration data for the usage.
   * @param {ItemUseOptions} [options]           Options used for configuring item usage.
   * @returns {Promise<ChatMessage|object|void>} Chat message if options.createMessage is true, message data if it is
   *                                             false, and nothing if the roll wasn't performed.
   */
  async use(config = {}, options = {}) {
    // Ensure the options object is ready
    options = foundry.utils.mergeObject({
      configureDialog: true,
      createMessage: true,
      flags: {},
      targets: game.user.targets
    }, options);

    console.log("A·C·E | ACE_Item.use():", this, config, options);

    // Reference aspects of the item data necessary for usage
    const resource = this.system.consume || {};        // Resource consumption

    // Define follow-up actions resulting from the item usage
    config = foundry.utils.mergeObject({
      createMeasuredTemplate: this.hasAreaTarget,
      consumeQuantity: this.system.uses?.autoDestroy ?? false,
      consumeRecharge: !!this.system.recharge?.value,
      consumeResource: !!resource.target && (!this.hasAttack || (resource.type !== "ammunition")),
      consumeUsage: !!this.system.uses?.per
    }, config);

    // Display a configuration dialog to customize the usage
    if (config.needsConfiguration === undefined) {
      config.needsConfiguration = config.createMeasuredTemplate
        || config.consumeRecharge || config.consumeResource || config.consumeSpellSlot || config.consumeUsage;
    }

    /**
     * A hook event that fires before an item usage is configured.
     * @function ACE.preUseItem
     * @memberof hookEvents
     * @param {ACE_Item} item                Item being used.
     * @param {ItemUseConfiguration} config  Configuration data for the item usage being prepared.
     * @param {ItemUseOptions} options       Additional options used for configuring item usage.
     * @returns {boolean}                    Explicitly return `false` to prevent item from being used.
     */
    if ( Hooks.call("ACE.preUseItem", this, config, options) === false ) return;

    /**
     * A hook event that fires before an item's resource consumption has been calculated.
     * @function ACE.preItemUsageConsumption
     * @memberof hookEvents
     * @param {ACE_Item} item                Item being used.
     * @param {ItemUseConfiguration} config  Configuration data for the item usage being prepared.
     * @param {ItemUseOptions} options       Additional options used for configuring item usage.
     * @returns {boolean}                    Explicitly return `false` to prevent item from being used.
     */
    if ( Hooks.call("ACE.preItemUsageConsumption", this, config, options) === false ) return;

    // Determine whether the item can be used by testing for resource consumption
    const usage = this._getUsageUpdates(config);
    if ( !usage ) return;

    /**
     * A hook event that fires after an item's resource consumption has been calculated but before any
     * changes have been made.
     * @function ACE.itemUsageConsumption
     * @memberof hookEvents
     * @param {ACE_Item} item                   Item being used.
     * @param {ItemUseConfiguration} config     Configuration data for the item usage being prepared.
     * @param {ItemUseOptions} options          Additional options used for configuring item usage.
     * @param {object} usage
     * @param {object} usage.actorUpdates       Updates that will be applied to the actor.
     * @param {object} usage.itemUpdates        Updates that will be applied to the item being used.
     * @param {object[]} usage.resourceUpdates  Updates that will be applied to other items on the actor.
     * @returns {boolean}                       Explicitly return `false` to prevent item from being used.
     */
    if ( Hooks.call("ACE.itemUsageConsumption", this, config, options, usage) === false ) return;

    // Commit pending data updates
    const { actorUpdates, itemUpdates, resourceUpdates } = usage;
    if ( !foundry.utils.isEmpty(itemUpdates) ) await this.update(itemUpdates);
    if ( config.consumeQuantity && (this.system.quantity === 0) ) await this.delete();
    if ( !foundry.utils.isEmpty(actorUpdates) ) await this.actor.update(actorUpdates);
    if ( resourceUpdates.length ) await this.actor.updateEmbeddedDocuments("Item", resourceUpdates);

    // Prepare card data & display it if options.createMessage is true
    const cardData = await this.displayCard(options);

    // Initiate measured template creation
    let templates;
    if ( config.createMeasuredTemplate ) {
      try {
        templates = await (ACE.canvas.CharacteristicTemplate.fromItem(this))?.drawPreview();
      } catch(err) {}
    }

    /**
     * A hook event that fires when an item is used, after the measured template has been created if one is needed.
     * @function ACE.useItem
     * @memberof hookEvents
     * @param {ACE_Item} item                              Item being used.
     * @param {ItemUseConfiguration} config                Configuration data for the roll.
     * @param {ItemUseOptions} options                     Additional options for configuring item usage.
     * @param {MeasuredTemplateDocument[]|null} templates  The measured templates if they were created.
     */
    Hooks.callAll("ACE.useItem", this, config, options, templates ?? null);

    // Create or return the Chat Message data
    return cardData;
  }

  /* -------------------------------------------- */

  /**
   * Verify that the consumed resources used by an Item are available and prepare the updates that should
   * be performed. If required resources are not available, display an error and return false.
   * @param {ItemUseConfiguration} config  Configuration data for an item usage being prepared.
   * @returns {object|boolean}             A set of data changes to apply when the item is used, or false.
   * @protected
   */
  _getUsageUpdates({
    consumeQuantity, consumeRecharge, consumeResource, consumeUsage
  }) {
    const actorUpdates = {};
    const itemUpdates = {};
    const resourceUpdates = [];

    // Consume Recharge
    if ( consumeRecharge ) {
      const recharge = this.system.recharge || {};
      if ( recharge.charged === false ) {
        ui.notifications.warn(game.i18n.format("ACE.ItemNoUses", {name: this.name}));
        return false;
      }
      itemUpdates["system.recharge.charged"] = false;
    }

    // Consume Limited Resource
    if ( consumeResource ) {
      const canConsume = this._handleConsumeResource(itemUpdates, actorUpdates, resourceUpdates);
      if ( canConsume === false ) return false;
    }

    // Consume Limited Usage
    if ( consumeUsage ) {
      const uses = this.system.uses || {};
      const available = Number(uses.value ?? 0);
      let used = false;
      const remaining = Math.max(available - 1, 0);
      if ( available >= 1 ) {
        used = true;
        itemUpdates["system.uses.value"] = remaining;
      }

      // Reduce quantity if not reducing usages or if usages hit zero, and we are set to consumeQuantity
      if ( consumeQuantity && (!used || (remaining === 0)) ) {
        const q = Number(this.system.quantity ?? 1);
        if ( q >= 1 ) {
          used = true;
          itemUpdates["system.quantity"] = Math.max(q - 1, 0);
          itemUpdates["system.uses.value"] = uses.max ?? 1;
        }
      }

      // If the item was not used, return a warning
      if ( !used ) {
        ui.notifications.warn(game.i18n.format("ACE.ItemNoUses", {name: this.name}));
        return false;
      }
    }

    // Return the configured usage
    return {itemUpdates, actorUpdates, resourceUpdates};
  }


  /* -------------------------------------------- */

  /**
   * Handle update actions required when consuming an external resource
   * @param {object} itemUpdates        An object of data updates applied to this item
   * @param {object} actorUpdates       An object of data updates applied to the item owner (Actor)
   * @param {object[]} resourceUpdates  An array of updates to apply to other items owned by the actor
   * @returns {boolean|void}            Return false to block further progress, or return nothing to continue
   * @protected
   */
  _handleConsumeResource(itemUpdates, actorUpdates, resourceUpdates) {
    const consume = this.system.consume || {};
    if ( !consume.type ) return;

    // No consumed target
    const typeLabel = CONFIG.ACE.characteristicConsumptionTypes[consume.type];
    if ( !consume.target ) {
      ui.notifications.warn(game.i18n.format("ACE.ConsumeWarningNoResource", {name: this.name, type: typeLabel}));
      return false;
    }

    // Identify the consumed resource and its current quantity
    let resource = null;
    let amount = Number(consume.amount ?? 1);
    let quantity = 0;
    switch ( consume.type ) {
      case "attribute":
        resource = foundry.utils.getProperty(this.actor.system, consume.target);
        quantity = resource || 0;
        break;
      case "ammunition":
      case "material":
        resource = this.actor.items.get(consume.target);
        quantity = resource ? resource.system.quantity : 0;
        break;
      case "hitDice":
        const denom = !["smallest", "largest"].includes(consume.target) ? consume.target : false;
        resource = Object.values(this.actor.classes).filter(cls => !denom || (cls.system.hitDice === denom));
        quantity = resource.reduce((count, cls) => count + cls.system.levels - cls.system.hitDiceUsed, 0);
        break;
      case "charges":
        resource = this.actor.items.get(consume.target);
        if ( !resource ) break;
        const uses = resource.system.uses;
        if ( uses.per && uses.max ) quantity = uses.value;
        else if ( resource.system.recharge?.value ) {
          quantity = resource.system.recharge.charged ? 1 : 0;
          amount = 1;
        }
        break;
    }

    // Verify that a consumed resource is available
    if ( resource === undefined ) {
      ui.notifications.warn(game.i18n.format("ACE.ConsumeWarningNoSource", {name: this.name, type: typeLabel}));
      return false;
    }

    // Verify that the required quantity is available
    let remaining = quantity - amount;
    if ( remaining < 0 ) {
      ui.notifications.warn(game.i18n.format("ACE.ConsumeWarningNoQuantity", {name: this.name, type: typeLabel}));
      return false;
    }

    // Define updates to provided data objects
    switch ( consume.type ) {
      case "attribute":
        actorUpdates[`system.${consume.target}`] = remaining;
        break;
      case "ammunition":
      case "material":
        resourceUpdates.push({_id: consume.target, "system.quantity": remaining});
        break;
      case "hitDice":
        if ( ["smallest", "largest"].includes(consume.target) ) resource = resource.sort((lhs, rhs) => {
          let sort = lhs.system.hitDice.localeCompare(rhs.system.hitDice, "en", {numeric: true});
          if ( consume.target === "largest" ) sort *= -1;
          return sort;
        });
        let toConsume = consume.amount;
        for ( const cls of resource ) {
          const available = (toConsume > 0 ? cls.system.levels : 0) - cls.system.hitDiceUsed;
          const delta = toConsume > 0 ? Math.min(toConsume, available) : Math.max(toConsume, available);
          if ( delta !== 0 ) {
            resourceUpdates.push({_id: cls.id, "system.hitDiceUsed": cls.system.hitDiceUsed + delta});
            toConsume -= delta;
            if ( toConsume === 0 ) break;
          }
        }
        break;
      case "charges":
        const uses = resource.system.uses || {};
        const recharge = resource.system.recharge || {};
        const update = {_id: consume.target};
        if ( uses.per && uses.max ) update["system.uses.value"] = remaining;
        else if ( recharge.value ) update["system.recharge.charged"] = false;
        resourceUpdates.push(update);
        break;
    }
  }

  /* -------------------------------------------- */

  /**
   * Display the chat card for an Item as a Chat Message
   * @param {object} [options]                Options which configure the display of the item chat card
   * @param {string} [options.rollMode]       The message visibility mode to apply to the created card
   * @param {boolean} [options.createMessage] Whether to automatically create a ChatMessage document (if
   *                                          true), or only return the prepared message data (if false)
   * @returns {ChatMessage|object} Chat message if `createMessage` is true, otherwise an object
   *                               containing message data.
   */
  async displayCard(options = {}) {

    // Render the chat card template
    const token = this.actor.token;
    const templateData = {
      actor: this.actor.toObject(false),
      tokenId: token?.uuid || null,
      item: this.toObject(false),
      data: await this.getChatData(),
      labels: this.labels,
      hasAttack: this.hasAttack,
      isHealing: this.isHealing,
      hasDamage: this.hasDamage,
      isVersatile: this.isVersatile,
      hasSave: this.hasSave,
      hasAreaTarget: this.hasAreaTarget,
      isTool: this.type === "tool",
      hasCharacteristicCheck: this.hasCharacteristicCheck
    };

    const defender = [...options.targets].map(t => t.name).join(", ");

    console.log("A·C·E | displayCard():", token, templateData, defender);

    const html = await renderTemplate("systems/ACE/templates/chat/item-card.hbs", templateData);

    // Create the ChatMessage data object
    const chatData = {
      user: game.user.id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: html,
      flavor: game.i18n.format(this.system.flavor, {
        attacker: this.actor.name,
        defender: defender
      }) || this.name,
      speaker: ChatMessage.getSpeaker({ actor: this.actor, token }),
      flags: { "core.canPopout": true }
    };

    // If the Item was destroyed in the process of displaying its card - embed the item data
    // in the chat message
    if ((this.type === "consumable") && !this.actor.items.has(this.id)) {
      chatData.flags["ACE.itemData"] = templateData.item;
    }

    // Merge in the flags from options
    chatData.flags = foundry.utils.mergeObject(chatData.flags, options.flags);

    /**
     * A hook event that fires before an item chat card is created.
     * @function ACE.preDisplayCard
     * @memberof hookEvents
     * @param {ACE_Item} item             Item for which the chat card is being displayed.
     * @param {object} chatData         Data used to create the chat message.
     * @param {ItemUseOptions} options  Options which configure the display of the item chat card.
     */
    Hooks.callAll("ACE.preDisplayCard", this, chatData, options);

    // Apply the roll mode to adjust message visibility
    ChatMessage.applyRollMode(chatData, options.rollMode || game.settings.get("core", "rollMode"));

    // Create the Chat Message or return its data
    const card = (options.createMessage !== false) ? await ChatMessage.create(chatData) : chatData;

    /**
     * A hook event that fires after an item chat card is created.
     * @function ACE.displayCard
     * @memberof hookEvents
     * @param {ACE_Item} item              Item for which the chat card is being displayed.
     * @param {ChatMessage|object} card  The created ChatMessage instance or ChatMessageData depending on whether
     *                                   options.createMessage was set to `true`.
     */
    Hooks.callAll("ACE.displayCard", this, card);

    return card;
  }

  /* -------------------------------------------- */
  /*  Chat Cards                                  */
  /* -------------------------------------------- */

  /**
   * Prepare an object of chat data used to display a card for the Item in the chat log.
   * @param {object} htmlOptions    Options used by the TextEditor.enrichHTML function.
   * @returns {object}              An object of chat data to render.
   */
  async getChatData(htmlOptions = {}) {
    const system = this.toObject().system;
    const labels = this.labels;

    // Rich text description
    system.description.value = await TextEditor.enrichHTML(system.description.value, {
      async: true,
      relativeTo: this,
      rollData: this.getRollData(),
      ...htmlOptions
    });

    // Item type specific properties
    const props = [];
    const fn = this[`_${this.type}ChatData`];
    if (fn) fn.bind(this)(system, labels, props);

    // Equipment properties
    if (system.hasOwnProperty("equipped") && !["loot", "tool"].includes(this.type)) {
      props.push(
        game.i18n.localize(system.equipped ? "ACE.Equipped" : "ACE.Unequipped")
      );
    }

    // Characteristic activation properties
    if (system.hasOwnProperty("activation")) {
      props.push(
        labels.activation + (system.activation?.condition ? ` (${system.activation.condition})` : ""),
        labels.target,
        labels.range,
        labels.duration
      );
    }

    // Filter properties and return
    system.properties = props.filter(p => !!p);

    // Console.log("A·C·E | getChatData():", system, labels, props);

    return system;
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for consumable type items.
   * @param {object} data     Copy of item data being use to display the chat message.
   * @param {object} labels   Specially prepared item labels.
   * @param {string[]} props  Existing list of properties to be displayed. *Will be mutated.*
   * @private
   */
  _consumableChatData(data, labels, props) {
    props.push(
      CONFIG.ACE.consumableTypes[data.consumableType],
      `${data.uses.value}/${data.uses.max} ${game.i18n.localize(data.description)}`
    );
    data.hasCharges = data.uses.value >= 0;
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for equipment type items.
   * @param {object} data     Copy of item data being use to display the chat message.
   * @param {object} labels   Specially prepared item labels.
   * @param {string[]} props  Existing list of properties to be displayed. *Will be mutated.*
   * @private
   */
  _equipmentChatData(data, labels, props) {
    props.push(
      CONFIG.ACE.equipmentTypes[data.armor.type],
      labels.armor || null,
      data.stealth ? game.i18n.localize("ACE.StealthDisadvantage") : null
    );
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for weapon type items.
   * @param {object} data     Copy of item data being use to display the chat message.
   * @param system
   * @param {object} labels   Specially prepared item labels.
   * @param {string[]} props  Existing list of properties to be displayed. *Will be mutated.*
   * @private
   */
  _weaponChatData(system, labels, props) {
    // Console.log("A·C·E | _weaponChatData():", system, labels, props);
    props.push(
      CONFIG.ACE.weaponTypes[system.weaponType]
    );
  }

  /* -------------------------------------------- */
  /*  Item Rolls - Attack, Damage, Saves, Checks  */
  /* -------------------------------------------- */

  /**
   * Place an attack roll using an item (weapon, feat, spell, or equipment)
   * Rely upon the effectRoll logic for the core implementation
   *
   * @param {EffectRollConfiguration} options  Roll options which are configured and provided to the effectRoll function
   * @returns {Promise<EffectRoll|null>}       A Promise which resolves to the created Roll instance
   */
  async rollAttack(options={}) {
    const flags = this.actor.flags.ACE ?? {};
    if ( !this.hasAttack ) throw new Error("You may not make an Attack Check with this Item.");
    let title = `${this.name} - ${game.i18n.localize("ACE.AttackRoll")}`;

    // Get the parts and rollData for this item's attack
    const {parts, rollData} = this.getAttackToHit();

    // Handle ammunition consumption
    delete this._ammunition;
    let ammo = null;
    let ammoUpdate = [];
    const consume = this.system.consume;
    if ( consume?.type === "ammunition" ) {
      ammo = this.actor.items.get(consume.target);
      if ( ammo?.system ) {
        const q = ammo.system.quantity;
        const consumeAmount = consume.amount ?? 0;
        if ( q && (q - consumeAmount >= 0) ) {
          this._ammunition = ammo;
          title += ` [${ammo.name}]`;
        }
      }

      // Get pending ammunition update
      const usage = this._getUsageUpdates({consumeResource: true});
      if ( usage === false ) return null;
      ammoUpdate = usage.resourceUpdates ?? [];
    }

    // Flags
    const elvenAccuracy = (flags.elvenAccuracy
      && CONFIG.ACE.characterFlags.elvenAccuracy.abilities.includes(this.abilityMod)) || undefined;

    // Compose roll options
    const rollConfig = foundry.utils.mergeObject({
      parts,
      actor: this.actor,
      data: rollData,
      exceptional: this.getExceptionalThreshold(),
      title,
      flavor: title,
      elvenAccuracy,
      halflingLucky: flags.halflingLucky,
      dialogOptions: {
        width: 400,
        top: options.event ? options.event.clientY - 80 : null,
        left: window.innerWidth - 710
      },
      messageData: {
        "flags.ACE.roll": {type: "attack", itemId: this.id },
        speaker: ChatMessage.getSpeaker({actor: this.actor})
      }
    }, options);

    /**
     * A hook event that fires before an attack is rolled for an Item.
     * @function ACE.preRollAttack
     * @memberof hookEvents
     * @param {ACE_Item} item                  Item for which the roll is being performed.
     * @param {EffectRollConfiguration} config  Configuration data for the pending roll.
     * @returns {boolean}                    Explicitly return false to prevent the roll from being performed.
     */
    if ( Hooks.call("ACE.preRollAttack", this, rollConfig) === false ) return;

    const roll = await rolls.makeCheck(rollConfig);
    if ( roll === null ) return null;

    /**
     * A hook event that fires after an attack has been rolled for an Item.
     * @function ACE.rollAttack
     * @memberof hookEvents
     * @param {ACE_Item} item          Item for which the roll was performed.
     * @param {EffectRoll} roll         The resulting roll.
     * @param {object[]} ammoUpdate  Updates that will be applied to ammo Items as a result of this attack.
     */
    Hooks.callAll("ACE.rollAttack", this, roll, ammoUpdate);

    // Commit ammunition consumption on attack rolls resource consumption if the attack roll was made
    if ( ammoUpdate.length ) await this.actor?.updateEmbeddedDocuments("Item", ammoUpdate);
    return roll;
  }

  /* -------------------------------------------- */

  /**
   * Place a damage roll using an item (weapon, feat, spell, or equipment)
   * Rely upon the damageRoll logic for the core implementation.
   * @param {object} [config]
   * @param {MouseEvent} [config.event]    An event which triggered this roll, if any
   * @param {boolean} [config.exceptional]    Should damage be rolled as a exceptional hit?
   * @param {number} [config.spellLevel]   If the item is a spell, override the level for damage scaling
   * @param {boolean} [config.versatile]   If the item is a weapon, roll damage using the versatile formula
   * @param {DamageRollConfiguration} [config.options]  Additional options passed to the damageRoll function
   * @param config.exceptional
   * @returns {Promise<DamageRoll>}        A Promise which resolves to the created Roll instance, or null if the action
   *                                       cannot be performed.
   */
  async rollDamage({exceptional=false, event=null, spellLevel=null, versatile=false, options={}}={}) {
    if ( !this.hasDamage ) throw new Error("You may not make a Damage Roll with this Item.");
    const messageData = {
      "flags.ACE.roll": {type: "damage", itemId: this.id},
      speaker: ChatMessage.getSpeaker({actor: this.actor})
    };

    // Get roll data
    const dmg = this.system.damage;
    const parts = dmg.parts.map(d => d.roll);
    const rollData = this.getRollData();
    if ( spellLevel ) rollData.item.level = spellLevel;

    // Configure the damage roll
    const actionFlavor = game.i18n.localize(this.system.actionType === "healing" ? "ACE.Healing" : "ACE.DamageRoll");
    const title = `${this.name} - ${actionFlavor}`;
    const rollConfig = {
      actor: this.actor,
      exceptional: exceptional ?? event?.altKey ?? false,
      data: rollData,
      event: event,
      fastForward: event ? event.shiftKey || event.altKey || event.ctrlKey || event.metaKey : false,
      parts: parts,
      title: title,
      flavor: this.labels.damageTypes.length ? `${title} (${this.labels.damageTypes})` : title,
      dialogOptions: {
        width: 400,
        top: event ? event.clientY - 80 : null,
        left: window.innerWidth - 710
      },
      messageData
    };

    // Adjust damage from versatile usage
    if ( versatile && dmg.versatile ) {
      parts[0] = dmg.versatile;
      messageData["flags.ACE.roll"].versatile = true;
    }

    // Scale damage from up-casting spells
    const scaling = this.system.scaling;
    if ( (this.type === "spell") ) {
      if ( scaling.mode === "cantrip" ) {
        let level;
        if ( this.actor.type === "character" ) level = this.actor.system.details.level;
        else if ( this.system.preparation.mode === "innate" ) level = Math.ceil(this.actor.system.details.cr);
        else level = this.actor.system.details.spellLevel;
        this._scaleCantripDamage(parts, scaling.formula, level, rollData);
      }
      else if ( spellLevel && (scaling.mode === "level") && scaling.formula ) {
        this._scaleSpellDamage(parts, this.system.level, spellLevel, scaling.formula, rollData);
      }
    }

    // Add damage bonus formula
    const actorBonus = foundry.utils.getProperty(this.actor.system, `bonuses.${this.system.actionType}`) || {};
    if ( actorBonus.damage && (parseInt(actorBonus.damage) !== 0) ) {
      parts.push(actorBonus.damage);
    }

    // Only add the ammunition damage if the ammunition is a consumable with type 'ammunition'
    if ( this._ammunition && (this._ammunition.type === "consumable") && (this._ammunition.system.consumableType === "ammunition") ) {
      parts.push("@ammunition");
      rollData.ammunition = this._ammunition.system.damage.parts.map(p => p.roll).join("+");
      rollConfig.flavor += ` [${this._ammunition.name}]`;
      delete this._ammunition;
    }

    // Factor in extra exceptional damage dice from the Barbarian's "Brutal Critical"
    if ( this.system.actionType === "melee" ) {
      rollConfig.exceptionalBonusDice = this.actor.getFlag("ACE", "meleeCriticalDamageDice") ?? 0;
    }

    // Factor in extra weapon-specific exceptional damage
    if ( this.system.exceptional?.damage ) rollConfig.criticalBonusDamage = this.system.exceptional.damage;

    foundry.utils.mergeObject(rollConfig, options);

    /**
     * A hook event that fires before a damage is rolled for an Item.
     * @function ACE.preRollDamage
     * @memberof hookEvents
     * @param {ACE_Item} item                     Item for which the roll is being performed.
     * @param {DamageRollConfiguration} config  Configuration data for the pending roll.
     * @returns {boolean}                       Explicitly return false to prevent the roll from being performed.
     */
    if ( Hooks.call("ACE.preRollDamage", this, rollConfig) === false ) return;

    const roll = await damageRoll(rollConfig);

    /**
     * A hook event that fires after a damage has been rolled for an Item.
     * @function ACE.rollDamage
     * @memberof hookEvents
     * @param {ACE_Item} item      Item for which the roll was performed.
     * @param {DamageRoll} roll  The resulting roll.
     */
    if ( roll ) Hooks.callAll("ACE.rollDamage", this, roll);

    // Call the roll helper utility
    return roll;
  }

  /* -------------------------------------------- */
  /*  Chat Message Helpers                        */
  /* -------------------------------------------- */

  /**
   * Apply listeners to chat messages.
   * @param {HTML} html  Rendered chat message.
   */
  static chatListeners(html) {
    html.on("click", ".card-buttons button", this._onChatCardAction.bind(this));
    html.on("click", ".item-name", this._onChatCardToggleContent.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Handle execution of a chat card action via a click event on one of the card buttons
   * @param {Event} event       The originating click event
   * @returns {Promise}         A promise which resolves once the handler workflow is complete
   * @private
   */
  static async _onChatCardAction(event) {
    event.preventDefault();

    console.log("A·C·E | ACE_Item._onChatCardAction():", event);

    // Extract card data
    const button = event.currentTarget;
    button.disabled = true;
    const card = button.closest(".chat-card");
    const messageId = card.closest(".message").dataset.messageId;
    const message = game.messages.get(messageId);
    const action = button.dataset.action;

    // Validate permission to proceed with the roll
    const isTargetted = action === "save";
    if ( !( isTargetted || game.user.isGM || message.isAuthor ) ) return;

    // Recover the actor for the chat card
    const actor = await this._getChatCardActor(card);
    if ( !actor ) return;

    // Get the Item from stored flag data or by the item ID on the Actor
    const storedData = message.getFlag("ACE", "itemData");
    const item = storedData ? new this(storedData, {parent: actor}) : actor.items.get(card.dataset.itemId);
    if ( !item ) {
      const err = game.i18n.format("ACE.ActionWarningNoItem", {
        item: card.dataset.itemId,
        name: actor.name
      });
      return ui.notifications.error(err);
    }
    const spellLevel = parseInt(card.dataset.spellLevel) || null;

    // Handle different actions
    let targets;
    switch ( action ) {
      case "attack":
        await item.rollAttack({ event });
        break;
      case "damage":
      case "versatile":
        await item.rollDamage({
          exceptional: event.altKey,
          event: event,
          spellLevel: spellLevel,
          versatile: action === "versatile"
        });
        break;
      case "formula":
        await item.rollFormula({event, spellLevel}); break;
      case "save":
        targets = this._getChatCardTargets(card);
        for ( let token of targets ) {
          const speaker = ChatMessage.getSpeaker({scene: canvas.scene, token: token});
          await token.actor.rollAbilitySave(button.dataset.ability, { event, speaker });
        }
        break;
      case "toolCheck":
        await item.rollToolCheck({event}); break;
      case "placeTemplate":
        const template = ACE.canvas.AbilityTemplate.fromItem(item);
        if ( template ) template.drawPreview();
        break;
      case "abilityCheck":
        targets = this._getChatCardTargets(card);
        for ( let token of targets ) {
          const speaker = ChatMessage.getSpeaker({scene: canvas.scene, token: token});
          await token.actor.rollAbilityTest(button.dataset.ability, { event, speaker });
        }
        break;
    }

    // Re-enable the button
    button.disabled = false;
  }

  /* -------------------------------------------- */

  /**
   * Handle toggling the visibility of chat card content when the name is clicked
   * @param {Event} event   The originating click event
   * @private
   */
  static _onChatCardToggleContent(event) {
    // Console.log("A·C·E | _onChatCardToggleContent():", event);
    event.preventDefault();
    const header = event.currentTarget;
    const card = header.closest(".chat-card");
    const content = card.querySelector(".card-content");
    content.style.display = content.style.display === "none" ? "block" : "none";
  }

  /* -------------------------------------------- */

  /**
   * Get the Actor which is the author of a chat card
   * @param {HTMLElement} card    The chat card being used
   * @returns {Actor|null}        The Actor document or null
   * @private
   */
  static async _getChatCardActor(card) {

    // Case 1 - a synthetic actor from a Token
    if ( card.dataset.tokenId ) {
      const token = await fromUuid(card.dataset.tokenId);
      if ( !token ) return null;
      return token.actor;
    }

    // Case 2 - use Actor ID directory
    const actorId = card.dataset.actorId;
    return game.actors.get(actorId) || null;
  }

  /* -------------------------------------------- */

  /**
   * Get the Actor which is the author of a chat card
   * @param {HTMLElement} card    The chat card being used
   * @returns {Actor[]}            An Array of Actor documents, if any
   * @private
   */
  static _getChatCardTargets(card) {
    let targets = canvas.tokens.controlled.filter(t => !!t.actor);
    if ( !targets.length && game.user.character ) targets = targets.concat(game.user.character.getActiveTokens());
    if ( !targets.length ) ui.notifications.warn(game.i18n.localize("ACE.ActionWarningNoToken"));
    return targets;
  }

  /* -------------------------------------------- */

}
