import { ACE_ItemSheet } from "./item.mjs";

export class ACE_EquipmentSheet extends ACE_ItemSheet {
  /**
   * Define default rendering options for the Equipment sheet.
   * @returns {object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "item", "equipment"]
    });
  }
}
