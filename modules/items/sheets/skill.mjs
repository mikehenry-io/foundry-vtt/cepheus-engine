import { ACE_ItemSheet } from "./item.mjs";

export class ACE_SkillSheet extends ACE_ItemSheet {
  /**
   * Define default rendering options for the Skill sheet.
   * @returns {object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["ACE", "sheet", "item", "skill"],
      width: 560,
      height: 400
    });
  }
}
